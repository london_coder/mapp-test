This solution satisfies the requirements specified in the requirement document:
'bean_counter_acceptance_criteria.txt'

There was no stated requirement to make this solution a runable application, but 
to do so would require little change.

The tests can be run via sbt, using the command 'sbt test'
All 3 tests pass green.

The code is checked in to git.

If this example solution was to be changed to be a runable application, then it 
would make sense to place the run time actor config in an external configuration.
The default Application.conf is not part of this exercise, but would be simple, 
and easy to create.

