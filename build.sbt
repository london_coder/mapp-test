name := "mapp-test"

version := "1.0"

scalaVersion := "2.11.8"


libraryDependencies ++= {
  val scalaLibVersion = scalaVersion.value
  val akkaLibVersion = "2.3.12"
  val scalatestVersion  = "3.0.1"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaLibVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaLibVersion % Test,
    "org.scalatest" %% "scalatest" % scalatestVersion % Test,
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % Test
  )
}