package britishgas
package exercise

import akka.actor._
import akka.event.Logging


/**
 * Companion object...
 * The object, as can be seen, contains the message types as case classes.
 */
object ClientActor {
	def props() = Props(new ClientActor)
	def propsAlt() = Props(classOf[ClientActor])

	case class BeanCountRequest()
	case class AddBeans(n: Int)
	case class TomatoRequest()
}

/**
 * Simple client actor to receive and return messages to the sender.
 */
class ClientActor extends Actor {
	val log = Logging(context.system, this)

	private var myBeans: Int = 0

	import ClientActor.{BeanCountRequest, AddBeans, TomatoRequest}

	def receive = {
		case BeanCountRequest =>
			log.info("Received: Bean Count Request")
			sender ! myBeans
		case AddBeans(n: Int) =>
			log.info(s"Received: Add Beans Request. Adding $n Beans")
			myBeans += n
	}

	override def unhandled(msg: Any) = msg match {
		case TomatoRequest => 
			log.info("Received a message I don't understand") 
			sender ! "Can't count, won't count tomatoes. I only count beans"
		case _ => 
			log.error("Received a message I cannot resspond to. Delegating to parent...")
			super.unhandled(msg) 
	}
}
