package britishgas
package exercise

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.{FeatureSpecLike, GivenWhenThen}
import scala.concurrent.duration._
/**
 * Unit tests as specified in the acceptance criteria document.
 */
class ClientActorTest extends TestKit(ActorSystem("testSystem"))
	with ImplicitSender
	with FeatureSpecLike 
	with GivenWhenThen {
	
	import ClientActor.{AddBeans, BeanCountRequest, TomatoRequest}
	import scala.language.postfixOps

	val actorRef = TestActorRef[ClientActor]

	feature("Actor test") {
		scenario("As a client of the bean counter actor") {
			Given("I have 0 beans")
			When("I request a count of those beans from the bean counter actor")
				actorRef ! BeanCountRequest
			Then("I receive a count with the value 0 from the bean counter actor")
				expectMsg(0)
		}
		scenario("I am a client of the bean counter actor") {
			Given("I have n > 0 beans")
				actorRef ! AddBeans(5)
			When("I request a count of those beans from the bean counter actor")
				actorRef ! BeanCountRequest
			Then("I receive a count with the value n from the bean counter actor")
				expectMsg(5)
		}
		scenario("Again as a client of the bean counter actor") {
			Given("I have 5 tomatoes")
			When("I request a count of those tomatoes from the bean counter actor")
				actorRef ! TomatoRequest
			Then("I receive a message with the value 'Can't count, won't count tomatoes. I only count beans' ")
				expectMsg("Can't count, won't count tomatoes. I only count beans")
		}
		scenario("Being a client of the bean counter actor") {
			Given("The actor only deals with beans")
			When("I send a message unrelated to beans")
				actorRef ! "I like trains"
			Then("I don't expect a response")
				expectNoMsg(200 millis)
		}
	}

	def afterAll: Unit = TestKit.shutdownActorSystem(system) 
}